// Demo init folder project
// cd existing_folder
// git init --initial-branch=main
// git remote add origin https://gitlab.com/phamthuy1706/git-3.git
// git add .
// git commit -m "Initial commit"
// git push -u origin main

// git status - kiểm tra các file thay đổi
// git diff - kiểm tra các thay đổi
// git clone - lấy về local project trên git lab server
// lệnh: "git clone LINk" (LINK là HTTPS Hoặc link SSH key)
// git add - Thêm các file đã thay đổi 
// "git add ." thêm tất cả
// "git add PATH" thêm 1 file
// git commit - lệnh định danh thay đổi
// "git commit -m "TÊN""
// git push - Lệnh đẩy các thay đổi lên git server
// "git push origin BRANCH_NAME"
// "git checkout -b "TÊN_BRANCH" - Lệnh tạo 1 nhánh mới
// "git checkout 'Tên branch'" - Lênh switch sang 1 nhánh khách 